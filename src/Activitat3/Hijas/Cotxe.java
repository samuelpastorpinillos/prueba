package Activitat3.Hijas;

public class Cotxe extends Vehiculo {

    protected float carburant;

    private int matricula;

    public Cotxe(int velocitat, float carburant, int matricula) {
        super(velocitat);
        this.carburant = carburant;
        this.matricula = matricula;
    }

    @Override
    public void accelerar() {
        super.accelerar();
        carburant -=0.5;
    }

    public float getCarburant() {
        return carburant;
    }

    public void proveir(int quantitat) {
        carburant += quantitat;
    }
}
