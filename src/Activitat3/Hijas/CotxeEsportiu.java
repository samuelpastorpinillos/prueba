package Activitat3.Hijas;

public class CotxeEsportiu extends Cotxe {

    private boolean descapotable;


    public CotxeEsportiu(int velocitat, float carburant, boolean descapotable, int matricula) {
        super(velocitat, carburant,matricula);
        this.descapotable = descapotable;
      }
    @Override
    public void accelerar() {
        super.accelerar();
        this.carburant -= 1;
    }

    @Override
    public float getCarburant() {
        return super.getCarburant();
    }
}

