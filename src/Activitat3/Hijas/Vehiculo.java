package Activitat3.Hijas;

public class Vehiculo {
    private int velocitat;
    private int rodes;

    public Vehiculo(int velocitat) {
        this.velocitat=velocitat;
        this.rodes= 4;

    }

    public void accelerar() {
        this.velocitat += 10;
    }

    public void frenar() {
        this.velocitat = 0;
    }
}
