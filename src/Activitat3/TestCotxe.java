package Activitat3;

import Activitat3.Hijas.Cotxe;
import Activitat3.Hijas.CotxeEsportiu;

public class TestCotxe {
    public static void main(String[] args) {
        CotxeEsportiu McQueen = new CotxeEsportiu(120,0,true,45664);
        Cotxe Mate = new Cotxe(134,0,324543534);

        McQueen.proveir(50);
        Mate.proveir(50);


        for (int i = 0; i < 5; i++) {
            McQueen.accelerar();
            Mate.accelerar();
        }

        McQueen.frenar();
        Mate.frenar();
        System.out.println(McQueen.getCarburant());
        System.out.println(Mate.getCarburant());
    }
}
