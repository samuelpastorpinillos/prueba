package activitat14;

import activitat14.Varios.Color;
import activitat14.Varios.Energetico;
import activitat14.subclase.Conectable;

public class Electrodomestico implements Conectable {
    protected double precioBase;
    private String marca;
    private String modelo;
    private Color color;
    private Energetico energetico;
    private int peso;
    protected String codigo;
    private int stock;
    private boolean estoyEncendido;


    public Electrodomestico(String codigo, double precioBase, String marca, String modelo, Color color, Energetico energetico, int peso) {
        this.precioBase = precioBase;
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.energetico = energetico;
        this.peso = peso;
        this.codigo = codigo;
        this.stock = 0;
    }

    public Electrodomestico(String codigo, String marca, String modelo) {
        this.precioBase = 100;
        this.marca = marca;
        this.modelo = modelo;
        this.color = Color.blanco;
        this.energetico = Energetico.F;
        this.peso = 5;
        this.codigo = codigo;
        this.stock=0;
    }



    public void obtenerPrecioVenta(){
        if (this.energetico.equals(Energetico.A)){
            this.precioBase = this.precioBase +100;
        }else if(this.energetico.equals(Energetico.B)){
                this.precioBase = this.precioBase +80;
        }else if(this.energetico.equals(Energetico.C)){
            this.precioBase = this.precioBase +60;
        }else if(this.energetico.equals(Energetico.D)){
            this.precioBase = this.precioBase +50;
        }else if(this.energetico.equals(Energetico.E)){
            this.precioBase = this.precioBase +30;
        }else if(this.energetico.equals(Energetico.F)){
            this.precioBase = this.precioBase +10;
        }
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return
                "precioBase=" + precioBase +
                ", marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", color=" + color +
                ", energetico=" + energetico +
                ", peso=" + peso +
                ", codigo=" + codigo +
                ", stock=" + stock +
                ", encendido= " + estoyEncendido+" ";
    }

    @Override
    public void encender() {
this.estoyEncendido=true;
    }

    @Override
    public void apagar() {
this.estoyEncendido=false;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Electrodomestico)){
            return false;
        }
        Electrodomestico electrodomestico = ((Electrodomestico) obj);
        return this.codigo.equals(electrodomestico.codigo);
    }
}
