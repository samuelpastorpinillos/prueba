package activitat14;

import activitat14.Varios.Color;
import activitat14.Varios.Energetico;
import activitat14.subclase.Computador;
import activitat14.subclase.Lavadora;
import activitat14.subclase.Television;
import activitat14.subclase.TomaCorriente;

public class TestTomaCorriente {
    public static void main(String[] args) {
        Computador computador1 = new Computador(16,3,256,300,1,400,"1");
        Computador computador2 = new Computador(16,3,256,300,2,400,"2");
        Lavadora lavadora1 = new Lavadora("04343","1","5");
        Lavadora lavadora2 = new Lavadora("04343","2","5");
        Television television1 = new Television("A1", 100,"Lg","HDQ5", Color.blanco, Energetico.F,5,1,true);
        Television television2 = new Television("A2", 100,"Lg","HDQ5", Color.blanco, Energetico.F,5,1,true);
        System.out.println(television1.toString());
        System.out.println(lavadora1.toString());

        TomaCorriente tomaCorriente = new TomaCorriente();
        tomaCorriente.conectar(computador1);
        tomaCorriente.conectar(television1);
        System.out.println(television1.toString());

    }
}
