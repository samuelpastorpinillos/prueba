package activitat14.subclase;

import activitat14.Electrodomestico;
import activitat14.Varios.Color;
import activitat14.Varios.Energetico;

public class Computador implements Conectable{
    private int ram;
    private int cpu;
    private int discoDuro;
    private int precioBase;
    private int numSerie;
    private int precioVenta;
    private boolean estoyEncendido;
    private String codigo;

    public Computador(int ram, int cpu, int discoDuro, int precioBase, int numSerie, int precioVenta,String codigo) {
        this.ram = ram;
        this.cpu = cpu;
        this.discoDuro = discoDuro;
        this.precioBase = precioBase;
        this.numSerie = numSerie;
        this.precioVenta = precioVenta;
        this.codigo=codigo;
    }

    @Override
    public void encender() {
        this.estoyEncendido = true;

    }

    @Override
    public void apagar() {
this.estoyEncendido= false;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Computador)){
            return false;
        }
        Computador computador = ((Computador) obj);
        return this.codigo.equals(computador.codigo);
    }

    @Override
    public String toString() {
        return "Computador"+super.toString();
    }
}

