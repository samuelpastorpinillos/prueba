package activitat14.subclase;

public interface Conectable {
    public void encender();

    public void apagar();
}
