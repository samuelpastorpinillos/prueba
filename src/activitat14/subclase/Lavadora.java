package activitat14.subclase;

import activitat14.Electrodomestico;
import activitat14.Varios.Color;
import activitat14.Varios.Energetico;


public class Lavadora extends Electrodomestico {
    private int carga;



    public Lavadora(String codigo, String marca, String modelo) {
        super(codigo, marca, modelo);
        this.carga=5;
    }

    public Lavadora(String codigo, double precioBase, String marca, String modelo, Color color, Energetico energetico, int peso, int carga) {
        super(codigo, precioBase, marca, modelo, color, energetico, peso);
        this.carga = carga;
    }

    @Override
    public String toString() {
        return "[LAVADORA]"+super.toString()+"Carga : "+carga;
    }

    @Override
    public void obtenerPrecioVenta() {
        super.obtenerPrecioVenta();
        if(carga > 30) {
            precioBase = precioBase +50;
        }
    }
}
