package activitat14.subclase;

import activitat14.Electrodomestico;
import activitat14.Varios.Color;
import activitat14.Varios.Energetico;

public class Television extends Electrodomestico {
    private int resolucion;
    private boolean smartTV;

    public Television(String codigo, double precioBase, String marca, String modelo, Color color, Energetico energetico, int peso, int resolucion, boolean smartTV) {
        super(codigo, precioBase, marca, modelo, color, energetico, peso);
        this.resolucion = resolucion;
        this.smartTV = smartTV;
    }

    public Television(String codigo, String marca, String modelo) {
        super(codigo, marca, modelo);
        this.resolucion = 20;
        this.smartTV = false;
    }

    @Override
    public void obtenerPrecioVenta() {
        super.obtenerPrecioVenta();
        if(resolucion > 40){
           this.precioBase = precioBase+precioBase * 0.3;
        }if (smartTV ==true){
            precioBase = precioBase + 50;
        }
    }

    @Override
    public String toString() {
        return "[TELEVISIÓN]" + super.toString() +"resolucion :"+resolucion+" , smartTV :"+smartTV;
    }
}
