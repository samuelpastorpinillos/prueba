package activitat14.subclase;

public class TomaCorriente{

    private Conectable [] conectables;

    public TomaCorriente() {
        this.conectables = new Conectable[10];
    }
    public boolean conectar(Conectable aparato) {

        for (int i = 0; i < conectables.length; i++) {
            if (conectables[i]!=null&&conectables[i].equals(aparato)) {
                return false;

            } else for (int j = 0; j < conectables.length; j++) {
                if (conectables[i]==null) {
                    conectables[i] = aparato;
                    aparato.encender();
                    return true;

                }
            }
        }
        return false;


}
    public boolean desconectar(Conectable aparato){
        for (int i = 0; i < conectables.length; i++) {
            if (conectables[i]!=null&&conectables[i].equals(aparato)) {
                conectables[i] = null;
                aparato.apagar();
                return true ;
            }
        }
        return false;
    }

}
