package activitat15;

import java.util.Random;

public class Coordenada {

    private int fila;
    private int columna;

    public Coordenada() {
        Random random = new Random();
        this.fila = random.nextInt(Tablero.DIMENSION) ;
        this.columna = random.nextInt(Tablero.DIMENSION) ;
    }
    public Coordenada(int fila, int columna) {
        this.fila = fila;
        this.columna = columna;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }
}
