package activitat15;

public enum EstadoCasilla {
    FICHA_O (){
        @Override
        public String toString() {
            return "\u2B55";
        }
    }, FICHA_X () {
        @Override
        public String toString() {
            return "\u274C";
        }
    }, VACIA () {
        @Override
        public String toString() {
            return "--";
        }
    }
}
