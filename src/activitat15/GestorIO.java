package activitat15;

import java.util.Scanner;

public class GestorIO {

    private Scanner scanner;

    public GestorIO() {
        scanner = new Scanner(System.in);
    }

    public int obtenerEntero(String mensaje) {
        do {
            System.out.print(mensaje);
            if (!scanner.hasNextInt()) {
                System.out.println("Debe introducir un entero");
                scanner.next();
            } else {
                return scanner.nextInt();
            }
        } while (true);
    }

    public int obtenerEntero(String mensaje, int min, int max) {
        do {
            int entero = obtenerEntero(mensaje);

            if (entero >= min && entero <= max) {
                return entero;
            }
            System.out.println("Error! Valores fuera de rango");

        } while (true);
    }


    public boolean confirmar(String mensaje) {
        System.out.print(mensaje);
        System.out.println(mensaje + " [S/N]");
        return scanner.next().toUpperCase().charAt(0) == 'S';
    }

}