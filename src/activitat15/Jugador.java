package activitat15;

public abstract class Jugador {

    protected EstadoCasilla color;
    protected JugadorPC jugarPC;
    protected JugadorManual jugarManual;

    public Jugador(EstadoCasilla color) {
        assert color == EstadoCasilla.FICHA_O || color == EstadoCasilla.FICHA_X;
        this.color = color;
    }

    public Jugador(EstadoCasilla color, JugadorPC jugarPC, JugadorManual jugarManual) {
        this.color = color;
        this.jugarPC = jugarPC;
        this.jugarManual = jugarManual;
    }

    public abstract void ponerFicha(Tablero tablero);

    protected Coordenada recogerCoordenadaValida(Tablero tablero) {
        do {
            Coordenada coordenada = recogerCoordenada();
            if (tablero.isOcupada(coordenada)) {
                System.out.println("Coordenada ocupada en el tablero");
            } else {
                return coordenada;
            }
        } while (true);
    }
      
    public void cantaVictoria() {
        System.out.println("CAMPEONESSS! Los " + color + " son los mejores");
    }

    private Coordenada recogerCoordenada() {
        GestorIO gestorIO = new GestorIO();

        int fila = gestorIO.obtenerEntero("Introduce fila [1-"+Tablero.DIMENSION+ "]: ", 1, Tablero.DIMENSION);
        int columna = gestorIO.obtenerEntero("Introduce columna [1-"+Tablero.DIMENSION+ "]: " ,1, Tablero.DIMENSION);

        return new Coordenada(fila - 1, columna - 1);


    }


}
