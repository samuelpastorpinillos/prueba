package activitat15;

public class JugadorManual extends Jugador{


    public JugadorManual(EstadoCasilla color) {
        super(color);
    }

    @Override
    public void ponerFicha(Tablero tablero) {
        assert tablero != null;
        assert !tablero.estaLleno();
        System.out.println("Tira el jugador con " + color);
        Coordenada coordenada = this.recogerCoordenadaValida(tablero);
        tablero.ponerFicha(coordenada, color);
    }
}
