package activitat15;

public class JugadorPC extends Jugador{


    public JugadorPC(EstadoCasilla color) {
        super(color);
    }

    @Override
    public void ponerFicha(Tablero tablero) {
        Coordenada coordenadaAleatoria = new Coordenada();
        if (!tablero.isOcupada(coordenadaAleatoria))
            tablero.ponerFicha(coordenadaAleatoria, this.color);
        }
    }

