package activitat15;

import java.util.Scanner;

public class Menu extends TresEnRaya {
     static Scanner scanner;

    public void mostrar() {

        // Repetir mientras no se introduzca la opción  4

        System.out.println(
                "1 - Jugador vs Ordenador\n" +
                        "2 - Jugador 1 vs Jugador 2\n" +
                        "3 - Ordenador vs Ordenador\n" +
                        "4 - Salir\n" +
                        "Seleccione una opción:");

        int opcion = seleccionaOpcion();

        ejecutarOpcion(opcion);

    }

    // Encargarse de recoger un número válido de opcion (entre 1 y 4)
    private int seleccionaOpcion() {
        Scanner scanner = new Scanner(System.in);
        int opcion = scanner.nextInt();

        do{

            if (opcion > 4 || opcion < 1) {
                System.out.println("ERROR! La opción seleccionada no es válida");
                mostrar();
                return opcion;

            }
            return opcion;
        } while (opcion != 4);

    }

    private void ejecutarOpcion(int opcion) {
        switch (opcion) {
            case 2:
                JugadorManual jugador1 = new JugadorManual(EstadoCasilla.FICHA_O);
                JugadorManual jugador2 = new JugadorManual(EstadoCasilla.FICHA_X);
                Partida partida = new Partida(jugador1,jugador2);
                partida.jugar();
                break;
            case 1:
                JugadorManual jugador3 = new JugadorManual(EstadoCasilla.FICHA_O);
                JugadorPC jugador4 = new JugadorPC(EstadoCasilla.FICHA_X);
                Partida partida2 = new Partida(jugador3, jugador4);
                partida2.jugar();
                break;
            case 3:
                JugadorPC jugador5 = new JugadorPC(EstadoCasilla.FICHA_O);
                JugadorPC jugador6 = new JugadorPC(EstadoCasilla.FICHA_X);
                Partida partida3 = new Partida(jugador5,jugador6);
                partida3.jugar();
                break;

        }

    }
}


