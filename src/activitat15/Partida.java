package activitat15;

public class Partida {
    private Tablero tablero;
    private Jugador[] jugadores;


    public  Partida(Jugador jugador, Jugador jugador2){
        jugadores = new Jugador[2];
        jugadores[0]=jugador;
        jugadores[1]=jugador2;
        tablero = new Tablero();

    }



    public void jugar(){
        int turno = 0;
        do {
            tablero.mostrar();
            jugadores[turno].ponerFicha(tablero);
            if (tablero.hayTresEnRaya()) {
                jugadores[turno].cantaVictoria();
                break;
            } else if(tablero.estaLleno()) {
                System.out.println("Empate");
                break;
            }
            turno = (turno + 1) % 2;
        } while (true);
        tablero.mostrar();
        tablero.vaciar();
    }

}
