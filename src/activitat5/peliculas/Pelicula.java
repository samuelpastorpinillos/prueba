package activitat5.peliculas;

import activitat5.util.Fecha;
import activitat5.util.Formato;

public class Pelicula extends Produccion {

    protected String actor;
    protected String actriu;

    public Pelicula(String titulo, int duracion, String descripcion, Fecha fecha, String actor, String actriu, Formato formato) {
        super(titulo, duracion, descripcion, fecha,formato);
        this.actor = actor;
        this.actriu = actriu;
        this.ser="Pelicula";
    }

    public Pelicula(String titulo, int duracion, String descripcion, Fecha fecha, String actor,Formato formato) {
        super(titulo, duracion, descripcion, fecha,formato);
        this.actor = actor;
        this.ser="Pelicula";
    }

    public Pelicula(String titulo, int duracion, String descripcion, Fecha fecha,Formato formato) {
        super(titulo, duracion, descripcion, fecha,formato);
        this.ser="Pelicula";
    }

    public Pelicula(String titulo, int duracion, String descripcion,String actriu, Fecha fecha,Formato formato) {
        super(titulo, duracion, descripcion, fecha,formato);
        this.actriu = actriu;
        this.ser="Pelicula";
    }

    public Pelicula(String titulo,  String descripcion, Fecha fecha,Formato formato) {
        super(titulo, 4800, descripcion, fecha,formato);
        this.ser="Pelicula";
    }


    @Override
    public String toString() {
        String toStringPadre = super.toString();
        return toStringPadre+", actor: ="+this.actor+", actriz= "+this.actriu;
    }

    @Override
    public void mostrarDetalle() {
        super.mostrarDetalle();
        System.out.println("Duración: "+duracion[0]+"h "+duracion[1]+"min "+duracion[2]+"s");
        System.out.println("--------------------------------------------");
    }
}
