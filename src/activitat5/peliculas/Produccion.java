package activitat5.peliculas;

import activitat5.util.Formato;
import activitat5.util.Fecha;

public class Produccion {
    private String titulo;
    private Formato formato;
    protected int [] duracion;
    private String descripcion;
    private Fecha fecha;
    protected String ser;

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int[] convertirTiempo(int segundos) {
        int horas = segundos / 3600;
        segundos %= 3600;
        int minutos = segundos / 60;
        segundos %= 60;
        int[] tiempo = {horas, minutos, segundos};
        return tiempo;
    }

    public void mostrarDetalle(){
        System.out.println("-----------------"+this.ser+"----------------");
        System.out.println(titulo+" ("+fecha.getAnyo()+")");
        System.out.println("Descripción: "+this.descripcion);


    }


    @Override
    public String toString() {
        return "Producción: título ="+this.titulo+", duración="+duracion[0]+"h "+duracion[1]+"min "+duracion[2]+", formato="+this.formato+"  ("+this.ser+") ";
    }


    public Produccion(String titulo, int duracion, String descripcion, Fecha fecha,Formato formato) {
        int  [] tiempo = convertirTiempo(duracion);
        this.titulo = titulo;
        this.duracion = tiempo;
        this.formato = formato;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.ser="Producción";


    }
}


