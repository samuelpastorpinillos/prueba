package activitat5.peliculas;

import activitat5.util.Fecha;
import activitat5.util.Formato;

public class Serie extends Produccion{

    protected int temporada;
    protected int capitulo;

    public Serie(String titulo, String descripcion, Fecha fecha, int temporada, int capitulo, Formato formato,int duracion) {
        super(titulo,duracion, descripcion, fecha,formato);
        this.temporada = temporada;
        this.capitulo = capitulo;
        this.ser="Serie";
    }

    @Override
    public String toString() {
        String toStringPadre = super.toString();
        return toStringPadre+", temporada=" + this.temporada +
                ", capítulos=" + this.capitulo;
    }

    @Override
    public void mostrarDetalle() {
        super.mostrarDetalle();
        System.out.println("Temporada "+temporada+" Capítulos: "+capitulo);
        System.out.println("Duración: "+duracion[0]+"h "+duracion[1]+"min "+duracion[2]+"s");
        System.out.println("--------------------------------------------");
    }
}
