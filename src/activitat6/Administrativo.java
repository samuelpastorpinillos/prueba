package activitat6;

public class Administrativo extends Empleado{
    private String despacho;
    private int numeroFax;

    public Administrativo(String nombre, String apellidos, String dni, String direccion, int telefono, int salario, String despacho, int numeroFax) {
        super(nombre, apellidos, dni, direccion, telefono, salario);
        this.despacho = despacho;
        this.numeroFax = numeroFax;
    }

    @Override
    public void incrementarSalario() {
        super.incrementarSalario(0.05);

    }

    @Override
    public String toString() {
        return "Puesto en la empresa: ADMINISTRATIVO ,"+super.toString()+", Oficina : "+despacho+", FAX : +"+numeroFax;
    }
}
