package activitat6;

public class Cliente{
    private String nombre;
    private String nombreCompanyia;
    private int telefono;

    public Cliente(String nombre, String nombreCompanyia, int telefono) {
        this.nombre = nombre;
        this.nombreCompanyia = nombreCompanyia;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }
}
