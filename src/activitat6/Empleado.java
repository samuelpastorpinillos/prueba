package activitat6;

public class Empleado {
    private String nombre;
    private String apellidos;
    private String dni;
    private String direccion;
    private int añosAntiguedad;
    private int telefono;
    protected double salario;


    public Empleado(String nombre, String apellidos, String dni, String direccion, int telefono, int salario) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.direccion = direccion;
        this.telefono = telefono;
        this.salario = salario;

    }

    public String toString() {
        return "nombre: " + nombre + ", apellidos: " + apellidos + ", dni: " + dni + ", telefono: "+telefono+", sueldo: " + salario;


    }

    public void incrementarSalario() {
       incrementarSalario(0.02);

    }
    protected void incrementarSalario(double incremento) {
        this.salario +=this.salario*incremento;
    }
}
