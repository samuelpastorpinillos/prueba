package activitat6;

public class Vendedor extends Empleado{

    private int matricula;
    private String marca;
    private String modelo;
    private String areaDeVenta;
    private Cliente [] listaClientes;
    private int contadorClientes = 0;
    private double comision;

    public Vendedor(String nombre, String apellidos, String dni, String direccion, int telefono, int salario, int matricula, String marca, String modelo, String areaDeVenta, double comision) {
        super(nombre, apellidos, dni, direccion, telefono, salario);
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
        this.areaDeVenta = areaDeVenta;
        this.listaClientes = new Cliente[10];
        this.comision = comision;


    }

    public void altaCliente(Cliente cliente ){
        for (int i = 0; i < this.listaClientes.length; i++) {
           if(this.listaClientes[i]==null) {
               this.listaClientes[i] = cliente;

           }
        }
        contadorClientes = contadorClientes +1;

    }
    public void bajaCliente(Cliente cliente){
        for (int i = 0; i <this.listaClientes.length ; i++) {
            if(listaClientes[i].getNombre().equals(cliente.getNombre())) {
                listaClientes[i] = null;
            }

        }
    }

    @Override
    public void incrementarSalario() {
        super.incrementarSalario(0.10);

    }

    @Override
    public String toString() {
        return "Puesto en la empresa: VENDEDOR ,"+super.toString()+", Numero Clientes: " + contadorClientes;
    }


}
