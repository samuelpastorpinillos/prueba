package activitat7;

import activitat8.Mascota;

public class Persona {
    private String nombre;

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public void saluda(){
        System.out.println("Hola que tal");
    }

    public void interactuar (Mascota mascota) {
        mascota.serAmigable();
        mascota.jugar();
    }
}
