package activitat8;

import activitat8.util.Comida;
import activitat8.util.Tamaño;

public abstract class Animal {
    private boolean vacunado;
    private Comida comida;
    private int hambre;
    private Tamaño tamaño;
    private String localización;
    protected String ser;

    public Animal(boolean vacunado, Comida comida, int hambre, Tamaño tamaño, String localización) {
        this.vacunado = false;
        this.comida = comida;
        this.hambre = 8;
        this.tamaño = tamaño;
        this.localización = localización;
    }

    public abstract void emitirSonido();

    public void comer() {
        if (comida == Comida.herbivoro) {
            this.hambre = this.hambre - 2;
        }
        if(comida == Comida.carnivoro) {
            this.hambre=this.hambre-1;
        }
        if(comida == Comida.omnivoro) {
            this.hambre=this.hambre-3;
        }
    }

    public void vacunar(){
        this.vacunado = true;
        System.out.println("VACUNANDO a un "+ser);
        this.emitirSonido();
    }

    @Override
    public String toString() {
        return "Animal{" +
                "vacunado=" + vacunado +
                ", comida=" + comida +
                ", hambre=" + hambre +
                ", tamaño=" + tamaño +
                ", localización='" + localización + '\'' +
                '}';
    }
}
