package activitat8;

import activitat8.animales.Leon;
import activitat8.util.Comida;
import activitat8.util.Tamaño;

public class TestAnimal {
    public static void main(String[] args) {
        System.out.println("---- Antes de vacunarlos y alimentarlos ------");
        Leon leon = new Leon(false, Comida.carnivoro,8, Tamaño.GRANDE,"La Sabana");
        System.out.println(leon.toString());
        Veterinario veterinario = new Veterinario();
        veterinario.vacunarAnimal(leon);
        leon.comer();
        System.out.println(leon.toString());


    }
}
