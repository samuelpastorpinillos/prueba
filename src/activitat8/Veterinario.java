package activitat8;

import activitat8.animales.Leon;
import activitat8.animales.Tigre;

public class Veterinario {
    public void vacunarAnimal(Animal animal) {
        if (animal instanceof Leon ) {
            animal.comer();
        }else if (animal instanceof Tigre) {
            animal.comer();
        }
        animal.vacunar();
    }
    public void alimentarAnimal (Animal animal) {
        animal.comer();
    }

}
