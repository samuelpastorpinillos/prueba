package activitat8.animales;

import activitat8.Animal;
import activitat8.Mascota;
import activitat8.util.Comida;
import activitat8.util.Tamaño;

public class Gato extends Animal implements Mascota {
    public Gato(boolean vacunado, Comida comida, int hambre, Tamaño tamaño, String localización) {
        super(vacunado, comida, hambre, tamaño, localización);
    }


    @Override
    public void emitirSonido() {
        System.out.println("AGGGGGGGGGGGGGHHH");
    }

    @Override
    public void serAmigable(
    ) {
        System.out.println("Ronroeno");

    }

    @Override
    public void jugar() {
        System.out.println("Zarpazo");
    }
}
