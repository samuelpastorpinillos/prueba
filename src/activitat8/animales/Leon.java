package activitat8.animales;

import activitat8.Animal;
import activitat8.util.Comida;
import activitat8.util.Tamaño;

public class Leon extends Animal {

    public Leon(boolean vacunado, Comida comida, int hambre, Tamaño tamaño, String localización) {
        super(vacunado, comida, hambre, tamaño, localización);
        ser = "leon";
    }

    @Override
    public void emitirSonido() {
        System.out.println("ARGGGGGGG");
    }


}
