package activitat8.animales;

import activitat8.Animal;
import activitat8.util.Comida;
import activitat8.util.Tamaño;

public class Tigre extends Animal {

    public Tigre(boolean vacunado, Comida comida, int hambre, Tamaño tamaño, String localización) {
        super(vacunado, comida, hambre, tamaño, localización);
    }

    @Override
    public void emitirSonido() {
        System.out.println("UATADADAgG");
    }
}
